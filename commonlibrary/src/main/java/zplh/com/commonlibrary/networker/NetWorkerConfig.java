package zplh.com.commonlibrary.networker;

import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import zplh.com.resourcelibrary.constants.NetConstant;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class NetWorkerConfig {
        String baseUrl;

        long outTime ;

        Converter.Factory factory;

        CallAdapter.Factory rxJava2CallAdapterFactory;

        public NetWorkerConfig setBaseUrl(String baseUrl) {
                this.baseUrl = baseUrl;
                return this;
        }

        public NetWorkerConfig setOutTime(long outTime) {
                this.outTime = outTime;
                return this;
        }

        public NetWorkerConfig setFactory(Converter.Factory factory) {
                this.factory = factory;
                return this;
        }

        public NetWorkerConfig setRxJava2CallAdapterFactory(CallAdapter.Factory rxJava2CallAdapterFactory) {
                this.rxJava2CallAdapterFactory = rxJava2CallAdapterFactory;
                return this;
        }


        public  static NetWorkerConfig getCustomConfig(){
                NetWorkerConfig netWorkerConfig = new NetWorkerConfig();
                netWorkerConfig.setBaseUrl(NetConstant.HOST_ADDRESS);
                netWorkerConfig.setOutTime(60000);
                netWorkerConfig.setFactory(CustomGsonConverterFactory.create());
                netWorkerConfig.setRxJava2CallAdapterFactory(RxJava2CallAdapterFactory.create());
                return netWorkerConfig;
        }
}
