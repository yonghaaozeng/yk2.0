package zplh.com.commonlibrary.networker;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class ApiException extends RuntimeException {
    private String mErrorCode;

    public ApiException(String errorCode, String errorMessage) {
        super(errorMessage);
       mErrorCode = errorCode;
    }


}
