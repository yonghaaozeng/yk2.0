package zplh.com.commonlibrary.networker;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class BaseBean {
    /**
     * code : 200
     * data : 0513
     * msg : Already exist
     */

    private String code = "";
    private String data = "";
    private String msg = "";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
