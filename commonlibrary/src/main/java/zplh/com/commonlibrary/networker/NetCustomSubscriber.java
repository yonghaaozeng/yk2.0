package zplh.com.commonlibrary.networker;


import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public abstract class NetCustomSubscriber<T> implements Observer<T> {




    @Override
    public  void onSubscribe(Disposable d){

    }

    @Override
    public abstract void onNext(T o);

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            // We had non-2XX http error
        } else if (e instanceof IOException) {
            // A network or conversion error happened
        } else if (e instanceof ApiException) {

        }
    }

    @Override
    public void onComplete() {
    }
}
