package zplh.com.commonlibrary.networker;

import android.text.TextUtils;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class HttpStatus {
    private String ret;
    private String msg;

    public String getRet() {
        return ret;
    }

    public HttpStatus setRet(String ret) {
        this.ret = ret;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public HttpStatus setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    /**
     * API是否请求失败
     *
     * @return 失败返回true, 成功返回false
     */
    public boolean isCodeInvalid() {
        return TextUtils.equals(ret, "400");
    }
}
