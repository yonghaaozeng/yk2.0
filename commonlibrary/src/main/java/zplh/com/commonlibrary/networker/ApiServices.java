package zplh.com.commonlibrary.networker;

import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by yong hao zeng on 2018/7/6.
 */
public interface ApiServices {
    @Streaming
    @GET
    Flowable<ResponseBody> getFile(@Url String url);
}
