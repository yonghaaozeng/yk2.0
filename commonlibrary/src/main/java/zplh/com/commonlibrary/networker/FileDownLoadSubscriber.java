package zplh.com.commonlibrary.networker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.ResponseBody;
import zplh.com.commonlibrary.utils.ShellUtils;

/**
 * Created by yong hao zeng on 2018/7/6.
 */
public abstract class FileDownLoadSubscriber extends DisposableSubscriber<ResponseBody> {
    private File file;

    public FileDownLoadSubscriber(File file) {
        this.file = file;
    }

    @Override
    public void onNext(ResponseBody body) {
        new WriteFile(file,body,this).execute();
    }

    @Override
    public void onError(Throwable t) {
        onFail("下载失败");
    }

    @Override
    public void onComplete() {

    }

    private class WriteFile  {
        private File file;
        private ResponseBody body;
        private FileDownLoadSubscriber subscriber;

        public WriteFile(File file, ResponseBody body, FileDownLoadSubscriber subscriber) {
            this.file = file;
            this.body = body;
            this.subscriber = subscriber;
        }

        protected void doInBackground(String... strings) {
            ShellUtils.execCommand("pm grant com.zplh.zplh_android_yk android.permission.WRITE_EXTERNAL_STORAGE",true);
            InputStream input = null;
            byte[] buf = new byte[2048];
            int len = 0;
            FileOutputStream fos = null;
            try{
                input = body.byteStream();
                final long total = body.contentLength();
                Float sum = 0f;
                File dir = file.getParentFile();
                if (!dir.exists()){
                    if (!dir.mkdirs()) onPostExecute(false);
                }
                fos = new FileOutputStream(file);
                while ((len = input.read(buf))!=-1){
                    sum += len;
                    fos.write(buf,0,len);
                    final Float finalSum = sum;
                    onProgressUpdate(finalSum, total);
                }
                fos.flush();
                onPostExecute(true);
            } catch (Exception ex){
                onPostExecute(false);
            }finally {
                try{
                    if (input != null) input.close();
                    if (fos != null) fos.close();
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }
        protected void onProgressUpdate(float... values) {
            subscriber.onProgress(values[0], values[1]);
        }

        protected void onPostExecute(Boolean result) {
            if (result){
                subscriber.onProgress(file.length(), file.length());
                subscriber.onSuccess(file);
            } else {
                subscriber.onFail("下载失败");
            }
        }

        public void execute() {
            doInBackground();
        }
    }

    public void onSuccess(File file){}

    public void onFail(String msg){}

    public void onProgress(float current,float total){}
}