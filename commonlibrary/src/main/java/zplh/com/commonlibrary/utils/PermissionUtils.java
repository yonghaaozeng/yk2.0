package zplh.com.commonlibrary.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by yong hao zeng on 2018/6/29.
 */
public class PermissionUtils {

    /**
     *
     * @return 是否有权限
     */
    public static boolean checkPermission(Context context,String permissionName){
        int i = context.checkSelfPermission(permissionName);
        if (i == PackageManager.PERMISSION_GRANTED)
            return  true;
        else return false;

    }

    public static void requestPermission(Activity context,String permissionName) {
        ActivityCompat.requestPermissions(context,new String[]{permissionName} ,20);

    }
}
