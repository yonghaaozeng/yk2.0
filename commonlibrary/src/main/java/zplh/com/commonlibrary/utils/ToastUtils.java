package zplh.com.commonlibrary.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
public class ToastUtils  {

    //错误提示
    @SuppressLint("CheckResult")
    public static void showToastError(Context context, String text){
        if (TextUtils.isEmpty(text)) return;
        Observable.just(text)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> Toasty.error(context,text, Toast.LENGTH_LONG,true).show());

    }

    //成功提示
    @SuppressLint("CheckResult")
    public static void showToastSuccess(Context context, String text){
        if (TextUtils.isEmpty(text)) return;
        Observable.just(text)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> Toasty.success(context,text, Toast.LENGTH_LONG,true).show());

    }


}
