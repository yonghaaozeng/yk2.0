package zplh.com.commonlibrary.utils;

import android.util.Base64;

import org.greenrobot.greendao.annotation.NotNull;

/**
 * Created by yong hao zeng on 2018/7/10.
 */
public class EncodedUtils {
    public static String getEncodedParam(String jsonObject) {
        return Base64.encodeToString(jsonObject.getBytes(), Base64.DEFAULT).replace("\n", "");
    }

    public static String decodeBase64(@NotNull String base64) throws Exception{
        byte[] decode = Base64.decode(base64, Base64.DEFAULT);
        return new String(decode);

    }
}
