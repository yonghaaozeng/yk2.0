package zplh.com.commonlibrary.utils;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import zplh.com.commonlibrary.LibraryManager;

/**
 * Created by yong hao zeng on 2018/7/4.
 */
public class LogUtils {
    public static void  init(){

        com.orhanobut.logger.Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public static void d(String tag,String content){
        if (!LibraryManager.DEBUG||content == null) return;
        Logger.t(tag).d(content);
    }

    public static void d(String tag,Object o){
        if (!LibraryManager.DEBUG||o == null) return;
        Logger.t(tag).d(o);
    }
}

