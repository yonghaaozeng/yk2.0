package zplh.com.commonlibrary.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;

/**
 * Created by yong hao zeng on 2018/7/18.
 */
public class AdbUtils {
    public static void  grant(String packageName,String permissionName){
        CommandResult commandResult = ShellUtils.execCommand("pm grant " + packageName + " " + permissionName, true);

    }

    public static void openWx(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(new ComponentName("com.tencent.mm","com.tencent.mm.ui.LauncherUI"));
        activity.startActivity(intent);
    }


    public static void startActivity(String activityName) {
        ShellUtils.execCommand("am start "+activityName, true);
    }
}

