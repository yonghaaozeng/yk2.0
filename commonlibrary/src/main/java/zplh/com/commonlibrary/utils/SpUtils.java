package zplh.com.commonlibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;

import zplh.com.commonlibrary.LibraryManager;

/**
 * Created by yong hao zeng on 2018/6/29.
 */
public class SpUtils  {

    public static String getValue(String spName, String key){
        SharedPreferences sharedPreferences = LibraryManager.getContext().getSharedPreferences(spName,
                Context.MODE_PRIVATE);

        return sharedPreferences.getString(key, "");

    }

    //为了简化 所有值都只能putString
    public static void putValue(String spName,String key,String value){
        SharedPreferences sharedPreferences = LibraryManager.getContext().getSharedPreferences(spName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(key, value);
        edit.apply();

    }
}
