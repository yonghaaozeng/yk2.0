package zplh.com.commonlibrary.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.lang.reflect.Method;

import zplh.com.commonlibrary.LibraryManager;

/**
 * 系统参数获取
 * Created by yong hao zeng on 2018/6/29.
 */
public class SystemParamerUtils  {

    public static String getImei(){

        //检查权限
        if (!PermissionUtils.checkPermission(LibraryManager.getContext()
                , Manifest.permission.READ_PHONE_STATE)){
        }else {
            ShellUtils.execCommand("pm grant zplh.com.zplh_android_yk android.permission.READ_PHONE_STATE",true );
        }

        //如果有权限 就直接从系统获取
        String imeiBySystem = getIMEIBySystem(LibraryManager.getContext());
        return imeiBySystem;

    }



    /**
     * 获取手机IMEI号
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
     public static String getIMEIBySystem(Context context) {
/*        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();*/
        String imeiList = "";
        imeiList = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId(TelephonyManager.PHONE_TYPE_GSM);
        if (TextUtils.isEmpty(imeiList)||imeiList.contains("00000")){

            return getImsi(context);
        }

        return imeiList;
    }

    /**
     * 获取手机IMsi号
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    public static String getImsi(Context context) {
/*        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();*/
        TelephonyManager manager= (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        Method method = null;
        String meid = "";
        try {
            method = manager.getClass().getMethod("getDeviceId", int.class);
             meid = (String) method.invoke(manager, 1);
            if (!meid.contains("A")){
                 meid = (String) method.invoke(manager, 2);
            }else {
                meid = manager.getDeviceId(TelephonyManager.PHONE_TYPE_GSM);
            }

            return meid;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    @SuppressLint("MissingPermission")
    public static String getIccid(Context context) {
     @SuppressLint("HardwareIds") String iccid =   ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getSimSerialNumber();
        return iccid;
    }
}
