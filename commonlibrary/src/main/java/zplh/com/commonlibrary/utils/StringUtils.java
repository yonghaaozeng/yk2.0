package zplh.com.commonlibrary.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yong hao zeng on 2018/7/2.
 */
public class StringUtils
{
    public static String findByReg(String reg,String put){
        Matcher matcher = Pattern.compile(reg)
                .matcher(put);
        boolean b = matcher
                .find();
        if (b){
            return matcher.group();

        }
        return "";
    }
}
