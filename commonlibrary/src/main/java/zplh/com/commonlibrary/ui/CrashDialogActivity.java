package zplh.com.commonlibrary.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import zplh.com.commonlibrary.R;
import zplh.com.commonlibrary.utils.ShellUtils;

/**
 * Created by yong hao zeng on 2018/7/12.
 */
public class CrashDialogActivity extends FragmentActivity {


    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crash_dialog_ui);
        Throwable msg = (Throwable) getIntent().getSerializableExtra("MSG");
        TextView tvTime = findViewById(R.id.tv_time);
        StringBuffer buffer = new StringBuffer();
        for (StackTraceElement stackTraceElement : msg.getStackTrace()) {
            buffer.append(stackTraceElement.getLineNumber()+":"+stackTraceElement.getClassName()+":"+stackTraceElement.getMethodName());
            buffer.append("\n");
        }
        ((TextView) findViewById(R.id.tv_msg)).setText(msg.getMessage()+"\n"+buffer.toString());
        Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe(aLong -> {
                        tvTime.setText(60-aLong+"秒后重启");
                    if (aLong == 60){
                        ShellUtils.execCommand("am start -n com.zplh.zplh_android_yk/com.zplh.zplh_android_yk.ui.BindingActivity",true);
                        finish();
                    }
                });
    }

}
