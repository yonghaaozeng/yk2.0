package zplh.com.commonlibrary;

import android.app.Application;
import android.content.Context;

import zplh.com.commonlibrary.utils.LogUtils;

/**
 * Created by yong hao zeng on 2018/6/29.
 */
public class LibraryManager {
    public static boolean DEBUG;
    private static Application context;

    //初始化library
    public static void init(Application applicationContext,boolean debug){
        context = applicationContext;
        LogUtils.init();
        DEBUG = debug;
    }

    public static Context getContext() {

        if (context == null)
            try {
                throw new Exception("没初始化 玩个球");
            } catch (Exception e) {
                e.printStackTrace();
            }

        return context;
    }
}
