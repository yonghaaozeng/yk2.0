package com.zplh.zplh_android_yk.dao;

import com.zplh.zplh_android_yk.bean.TaskBean;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import zplh.com.commonlibrary.LibraryManager;
import zplh.com.resourcelibrary.constants.DBConstant;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
public class DaoUtils {
    /**
     * 加载10条非定时数据
     * @return
     */
        public static List<TaskBean> load10(){
            TaskBeanDao taskBeanDao = getSession().getTaskBeanDao();
            QueryBuilder<TaskBean> queryBuilder = taskBeanDao.queryBuilder();
            queryBuilder.limit(30);
            Calendar  calendar = Calendar. getInstance();
            calendar.add( Calendar. DATE, -1); //获取前一天的时间
            Date date= calendar.getTime();
            long time = date.getTime();
            queryBuilder.where(TaskBeanDao.Properties.SendTime.gt(time)
            );
            List<TaskBean> list = queryBuilder.list();
            return list;
        }

        public static void add(TaskBean taskBean){
            getSession().getTaskBeanDao().insert(taskBean);
        }

      private static DaoSession getSession(){
          DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(LibraryManager.getContext(), DBConstant.DB_NAME, null);
          DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
          DaoSession daoSession = daoMaster.newSession();
            return daoSession;
      }

    public static void delete(TaskBean taskBean) {
        TaskBeanDao taskBeanDao = getSession().getTaskBeanDao();
        taskBeanDao.delete(taskBean);
        }

    public static void updata(TaskBean currentTaskbean) {
        TaskBeanDao taskBeanDao = getSession().getTaskBeanDao();
        taskBeanDao.update(currentTaskbean);
    }

    public static void clean() {
        TaskBeanDao taskBeanDao = getSession().getTaskBeanDao();
        taskBeanDao.deleteAll();

        }
}
