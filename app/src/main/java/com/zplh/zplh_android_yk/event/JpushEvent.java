package com.zplh.zplh_android_yk.event;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/08/21
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class JpushEvent {
        String alias;
        String satate;
        int type; //0 别名 1 状态

    public String getAlias() {
        return alias;
    }

    public JpushEvent setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    public String getSatate() {
        return satate;
    }

    public JpushEvent setSatate(String satate) {
        this.satate = satate;
        return this;
    }

    public int getType() {
        return type;
    }

    public JpushEvent setType(int type) {
        this.type = type;
        return this;
    }
}
