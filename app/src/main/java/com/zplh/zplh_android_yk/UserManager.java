package com.zplh.zplh_android_yk;

/**
 * Created by yong hao zeng on 2018/7/2.
 */
public class UserManager {
    private static String mid;
    private static String mImei;
    private static UserManager instance;
    private UserManager(){}

    public static UserManager getInstance(){
        if (instance == null){
            instance = new UserManager();
        }
        return instance;
    }

    public static UserManager init(String id){
        if (instance == null){
            instance = new UserManager();
        }
        mid = id;
        return instance;
    }


    public void setId(String id){
        mid = id;
    }
    public void setImei(String imei){
        mImei = imei;

    }

    public  String getImei() {
        return mImei;
    }

    public String getMid() {
        return mid;

    }
}
