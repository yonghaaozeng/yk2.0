package com.zplh.zplh_android_yk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.bean.TaskBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {
    Context context;
    List<TaskBean> taskList;
    ItemClicklistener mClicklistener;
    public TaskAdapter(Context context, List<TaskBean> taskList) {
        this.context = context;
        this.taskList = taskList;
    }




    public TaskAdapter setClicklistener(ItemClicklistener clicklistener) {
        mClicklistener = clicklistener;
        return this;
    }

    public void setTaskList(List<TaskBean> taskList) {
        this.taskList = taskList;
    }

    public Context getContext() {
        return context;
    }

    public TaskAdapter setContext(Context context) {
        this.context = context;
        return this;
    }

    public List<TaskBean> getTaskList() {
        return taskList;
    }

    public ItemClicklistener getClicklistener() {
        return mClicklistener;
    }

    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context)
                .inflate(R.layout.main_item, null);
       return new TaskViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        TaskBean taskBean = taskList.get(position);
        holder.tvName.setText(taskBean.getTaskName()+"("+taskBean.getScriptVersion()+")");
        holder.state.setBackgroundResource(taskBean.getStateIcon());
        holder.sendTime.setText(taskBean.getFormatTime()[0]);
        holder.endTime.setText(taskBean.getFormatTime()[1]);
        holder.tvId.setText(taskBean.getLogId());
        if (!TextUtils.isEmpty(taskBean.getStateMsg())){
            holder.tvStatemsg.setVisibility(View.VISIBLE);
            holder.tvStatemsg.setText(taskBean.getStateMsg());
        }else {
            holder.tvStatemsg.setVisibility(View.GONE);
            holder.tvStatemsg.setText("");
        }
        if (taskBean.getIsTimeTask()){
            holder.ivType.setImageResource(R.drawable.alarm);
        }else{
            holder.ivType.setImageResource(R.drawable.task);
        }
        holder.itemView.setOnClickListener(v -> {
            if (mClicklistener!=null) {
                mClicklistener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskList == null?0:taskList.size();
    }

    class TaskViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_state_msg)
        TextView tvStatemsg;
        @BindView(R.id.iv)
        ImageView ivType;
        @BindView(R.id.iv_state)
        View state;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_id)
        TextView tvId;
        @BindView(R.id.send_time)
        TextView sendTime;
        @BindView(R.id.end_time)
        TextView endTime;
        public TaskViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

 public    interface ItemClicklistener{
        void onItemClick(int position);


    }


}
