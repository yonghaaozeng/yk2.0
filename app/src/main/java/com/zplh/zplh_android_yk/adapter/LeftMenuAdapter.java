package com.zplh.zplh_android_yk.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.enumconstant.MenuEnum;

import java.util.List;

/**
 * Created by yong hao zeng on 2018/7/2.
 */
public class LeftMenuAdapter extends BaseAdapter {
    Context context;
    List<MenuEnum> datas;

    public LeftMenuAdapter(Context context, List<MenuEnum> datas) {
        this.context = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas == null?0:datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return datas.get(position).getMenuId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Viewholder viewholder;
        if (convertView == null){
            View inflate = LayoutInflater.from(context)
                    .inflate(R.layout.left_item, null, false);
            convertView = inflate;
            viewholder = new Viewholder(inflate);
           convertView.setTag(viewholder);

        }

        viewholder = (Viewholder) convertView.getTag();
        viewholder.itemView.setText(datas.get(position).getDes());
        Drawable icon = context.getDrawable(datas.get(position).getIcon());

        icon.setBounds(0, 0, 50, 50);
        viewholder.itemView.setCompoundDrawables(icon,null ,null ,null );


        return convertView;
    }

    public class Viewholder{
        public TextView itemView;

        public Viewholder(View rootView) {
            itemView = (TextView) rootView.findViewById(R.id.tv_name);

        }
    }

}
