package com.zplh.zplh_android_yk.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.bean.VersionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yong hao zeng on 2018/5/11/011.
 */
public class VersionAdapter extends BaseAdapter {
    private Context context;
    private List<VersionBean.DataBean> beans = new ArrayList<>();
    public VersionAdapter(Context context) {
        this.context = context;
    }

    public VersionAdapter setBeans(List<VersionBean.DataBean> beans) {
        this.beans = beans;
        return this;
    }

    @Override
    public int getCount() {
        return beans.size();
    }

    @Override
    public VersionBean.DataBean getItem(int position) {
        return beans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHodler;
        if (convertView == null){
                convertView = LayoutInflater.from(context).inflate(R.layout.item_version,null,false);
                viewHodler = new ViewHolder(convertView);
                convertView.setTag(viewHodler);
            }
        VersionBean.DataBean dataBean = beans.get(position);
        viewHodler = (ViewHolder) convertView.getTag();
        viewHodler.tv_time.setText(dataBean.getAdd_time()+"");
        viewHodler.tv_version.setText(dataBean.getVersion_number()+"");
        return convertView;
    }

    class ViewHolder {
        TextView tv_version;
        TextView tv_time;
        public ViewHolder(View rootView) {
            tv_version = (TextView) rootView.findViewById(R.id.tv_version_id);
            tv_time = (TextView) rootView.findViewById(R.id.tv_time);
        }
    }
}
