package com.zplh.zplh_android_yk.intf;


import com.zplh.zplh_android_yk.bean.WxPhoneBean;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by yong hao zeng on 2018/7/20.
 */
public interface GetPhone {
    @GET
    Observable<WxPhoneBean> getPhone(@Url String url, @QueryMap HashMap<String,String> par);
}
