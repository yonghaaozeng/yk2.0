package com.zplh.zplh_android_yk.intf;

import com.zplh.zplh_android_yk.bean.CheckImeiBean;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/07/23
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface SendReceive {
        @GET
        Observable<CheckImeiBean> sendReciver(@Url String url, @QueryMap HashMap<String,String> param);
}
