package com.zplh.zplh_android_yk.intf;

import com.zplh.zplh_android_yk.bean.BindingBean;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import zplh.com.resourcelibrary.constants.NetConstant;

/**
 * Created by yong hao zeng on 2018/7/2.
 */
public interface BindingServices {
        @GET(NetConstant.BINDING)
    Observable<BindingBean> getRxData(@QueryMap HashMap<String,String> map);

}
