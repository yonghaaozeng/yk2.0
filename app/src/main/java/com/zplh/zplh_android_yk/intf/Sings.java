package com.zplh.zplh_android_yk.intf;

import com.zplh.zplh_android_yk.bean.SingBean;

import io.reactivex.Observable;
import retrofit2.http.GET;
import zplh.com.resourcelibrary.constants.NetConstant;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/08/17
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface Sings {
    @GET(NetConstant.SING)
    Observable<SingBean> getSing();
}
