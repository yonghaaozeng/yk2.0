package com.zplh.zplh_android_yk.intf;

import com.zplh.zplh_android_yk.bean.CheckPhonelimitBean;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/09/11
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface CheckPhoneLimit {
    @GET
    Observable<CheckPhonelimitBean> checkLimit(@Url String url, @Query("account_id")String id );

}
