package com.zplh.zplh_android_yk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zplh.zplh_android_yk.ui.BindingActivity;

import zplh.com.commonlibrary.utils.LogUtils;


/**
 * Created by lichun on 2017/6/26.
 * Description://开机自启
 */

public class BootCompletedReceiver extends BroadcastReceiver {
    private final String ACTION_BOOT = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(final Context context, Intent intent) {
        if (ACTION_BOOT.equals(intent.getAction())) {
            LogUtils.d("BOOTCOMPLETE","开机启动");
            Intent start = new Intent(context, BindingActivity.class);
            start.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//
            context.startActivity(start);
                }
    }
}
