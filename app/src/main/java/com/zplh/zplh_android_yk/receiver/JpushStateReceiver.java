package com.zplh.zplh_android_yk.receiver;

import android.content.Context;

import com.zplh.zplh_android_yk.MyApplication;
import com.zplh.zplh_android_yk.event.JpushEvent;
import com.zplh.zplh_android_yk.utils.RxBus;

import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.service.JPushMessageReceiver;
import zplh.com.commonlibrary.utils.ToastUtils;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/08/21
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class JpushStateReceiver extends JPushMessageReceiver {
    @Override
    public void onAliasOperatorResult(Context context, JPushMessage message) {
        super.onAliasOperatorResult(context, message);
        ToastUtils.showToastSuccess(MyApplication.getCurrentActivity(),"极光已绑定:"+message.getAlias());
        JpushEvent event = new JpushEvent();
        event.setType(0);
        event.setAlias(message.getAlias());
        RxBus.getInstance().post(event);
    }
}
