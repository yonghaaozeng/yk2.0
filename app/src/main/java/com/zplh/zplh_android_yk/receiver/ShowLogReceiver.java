package com.zplh.zplh_android_yk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.zplh.zplh_android_yk.MyApplication;

import zplh.com.commonlibrary.utils.LogUtils;
import zplh.com.commonlibrary.utils.ToastUtils;

/**
 * Author：liaogulong
 * Time: 2018/7/2/002   10:54
 * Description：
 */
public class ShowLogReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            //接收键值对
            Bundle bundle = intent.getExtras();
            String message = bundle.getString("message");
            LogUtils.d("SCRIPT_STATE",message);
            //消息toast
            ToastUtils.showToastSuccess(MyApplication.getCurrentActivity(),message );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
