package com.zplh.zplh_android_yk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zplh.zplh_android_yk.ui.BindingActivity;

import zplh.com.commonlibrary.utils.LogUtils;

/**
 *
 * Description:更新app后自动打开
 */

public class UpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED")){
            Intent intent2 = new Intent(context, BindingActivity.class);
            LogUtils.d(" UpdateReceiver","当前程序已经替换");
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);

        }

    }
}
