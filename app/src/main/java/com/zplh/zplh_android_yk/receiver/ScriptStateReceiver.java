package com.zplh.zplh_android_yk.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.zplh.zplh_android_yk.MyApplication;
import com.zplh.zplh_android_yk.TaskManager;

import zplh.com.commonlibrary.utils.EncodedUtils;
import zplh.com.commonlibrary.utils.LogUtils;
import zplh.com.commonlibrary.utils.ToastUtils;

/**
 * Created by yong hao zeng on 2018/7/11.
 */
public class ScriptStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Bundle extras = intent.getExtras();
            if (extras == null) return;
            String log_id = extras.getString("log_id");//log_id
            String msg = extras.getString("msg");//错误原因
            String path = extras.getString("path");

            if (!TextUtils.isEmpty(path)){
                String s = EncodedUtils.decodeBase64(path);
                TaskManager.getInstance().setErrorPath(log_id,s);
            }
            try {
                String param = EncodedUtils.decodeBase64(msg);
                if (TextUtils.equals("��",param)){
                    throw new Exception();
                }
                LogUtils.d("SCRIPT_ERROR", msg);
                ToastUtils.showToastError(MyApplication.getCurrentActivity(), param);
                TaskManager.getInstance().setError(log_id,  param);
            }catch (Exception e){
                LogUtils.d("SCRIPT_ERROR", msg);
                ToastUtils.showToastError(MyApplication.getCurrentActivity(), msg);
                TaskManager.getInstance().setError(log_id, msg);
            }

            TaskManager.getInstance().stopTask();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
