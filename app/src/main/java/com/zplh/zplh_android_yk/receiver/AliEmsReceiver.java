package com.zplh.zplh_android_yk.receiver;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.sdk.android.push.MessageReceiver;
import com.alibaba.sdk.android.push.notification.CPushMessage;
import com.zplh.zplh_android_yk.TaskManager;
import com.zplh.zplh_android_yk.bean.MessageBean;
import com.zplh.zplh_android_yk.bean.TaskBean;
import com.zplh.zplh_android_yk.utils.MessageUtils;

import org.json.JSONObject;

import zplh.com.commonlibrary.utils.GsonUtils;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/10/26
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class AliEmsReceiver extends MessageReceiver {
    @Override
    protected void onMessage(Context context, CPushMessage message) {
        super.onMessage(context, message);
        if (message == null||TextUtils.isEmpty(message.getContent()))return;
        try {
            JSONObject jsonObject = new JSONObject(message.getContent());
            MessageBean.ContentBean.DataBean dataBean = GsonUtils.changeGsonToBean(jsonObject.get("data").toString(), MessageBean.ContentBean.DataBean.class);

            if (TextUtils.isEmpty(dataBean.getEntrance())) {
                return;
            }
            TaskBean taskBean = MessageUtils.message2Task(jsonObject.get("data").toString());
            if (taskBean ==null||TextUtils.isEmpty(taskBean.getMethodName())) return;
            TaskManager.getInstance().addTask(taskBean);
        }catch (Exception e){
            return;
        }

    }
}
