package com.zplh.zplh_android_yk.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.jaeger.library.StatusBarUtil;
import com.zplh.zplh_android_yk.R;

import butterknife.ButterKnife;

/**
 * Created by yong hao zeng on 2018/6/29.
 */
public abstract class BaseActivity extends AppCompatActivity {

    //是否第一次可见启动
    boolean isFirstLaunch = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        StatusBarUtil.setColor(this,getColor(R.color.colorPrimary));
    }

    //获取布局id
    abstract int getLayoutId();


    @Override
    protected void onStart() {
        super.onStart();
        if (isFirstLaunch) {
            onFirstStart();
            isFirstLaunch = false;
        }else {

        }
    }

    //第一次启动
    abstract void onFirstStart(

    );


}
