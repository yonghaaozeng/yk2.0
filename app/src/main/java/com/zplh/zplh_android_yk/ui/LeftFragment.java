package com.zplh.zplh_android_yk.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.zplh.zplh_android_yk.MyApplication;
import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.TaskManager;
import com.zplh.zplh_android_yk.UserManager;
import com.zplh.zplh_android_yk.adapter.LeftMenuAdapter;
import com.zplh.zplh_android_yk.bean.SingBean;
import com.zplh.zplh_android_yk.enumconstant.MenuEnum;
import com.zplh.zplh_android_yk.intf.Sings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;
import io.reactivex.Observable;
import zplh.com.commonlibrary.networker.NetCustomSubscriber;
import zplh.com.commonlibrary.networker.NetWorker;
import zplh.com.commonlibrary.utils.FileUtils;
import zplh.com.commonlibrary.utils.SpUtils;
import zplh.com.commonlibrary.utils.ToastUtils;
import zplh.com.resourcelibrary.constants.SpConstants;

import static android.content.Context.CLIPBOARD_SERVICE;
import static com.zplh.zplh_android_yk.enumconstant.MenuEnum.LOOK_ID;
import static com.zplh.zplh_android_yk.enumconstant.MenuEnum.SIGNATURE;
import static com.zplh.zplh_android_yk.enumconstant.MenuEnum.UNBIND;

/**
 * Created by yong hao zeng on 2018/7/2.
 */
public class LeftFragment extends BaseFragment {
    @BindView(R.id.lv)
    ListView lv;
    List<MenuEnum> datas = new ArrayList<>();
    private LeftMenuAdapter leftMenuAdapter;

    @Override
    int getLayoutId() {
        return R.layout.left_fragment;
    }

    @Override
    protected void onFragmentVisibleChange(boolean isVisible, View view) {
        if (isVisible) {
            if (leftMenuAdapter != null)
                lv.setAdapter(leftMenuAdapter);
            lv.setOnItemClickListener((AdapterView<?> parent, View view1, int position, long id) -> {
                switch (datas.get(position)){
                    case LOOK_ID:
                        ToastUtils.showToastSuccess(Objects.requireNonNull(getActivity()), UserManager.getInstance().getMid());
                    break;
                    case CLEAN_TASK:
                        TaskManager.getInstance().cleanTask();
                        FileUtils.deleteDir(Environment.getExternalStorageDirectory().getPath()+"/ERROR");
                        ToastUtils.showToastSuccess(Objects.requireNonNull(getActivity()),"清除成功");

                        break;
                    case VERSION_LIST:
                        Intent intent = new Intent();
                        intent.setClass(Objects.requireNonNull(getContext()),VersionActivity.class);
                        startActivity(intent);

                        break;
//                    case CLEAN_SCRIPT:
////                        String scritPath = Environment.getExternalStorageDirectory().getPath()+"/scripts";
////                        ShellUtils.execCommand("rm -rf " + scritPath, true);
////                        ToastUtils.showToastSuccess(Objects.requireNonNull(getActivity()),"清除成功");
//                        break;
                    case SIGNATURE:
                          ToastUtils.showToastSuccess(MyApplication.getCurrentActivity(),"获取中");

                        Observable<SingBean> sing = NetWorker.getInstance().getRetrofit()
                                .create(Sings.class)
                                .getSing();

                        NetWorker.getInstance().createNetAsync(sing, new NetCustomSubscriber<SingBean>() {
                            @Override
                            public void onNext(SingBean o) {
                                String content = o.getData().get(0).getContent();
                                if (TextUtils.isEmpty(content))return;
                                ClipboardManager mClipboardManager = (ClipboardManager) MyApplication.context.getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("签名", content);
                                mClipboardManager.setPrimaryClip(clipData);
                                ToastUtils.showToastSuccess(MyApplication.getCurrentActivity(),"获取签名成功,可粘贴");

                            }
                        });
                        break;
                    case UNBIND:
                        SpUtils.putValue(SpConstants.SYSTEM_PARAMETER_SP,SpConstants.MID,"");
                        JPushInterface.setAlias(getContext(),10086,"");
                        Intent intent1 = new Intent(getContext(),BindingActivity.class);
                        getActivity().startActivity(intent1);
                        getActivity().finish();
                        break;
                }

            });
        }
    }

    @Override
    protected void onFragmentFirstVisible() {
            initMenu();
        leftMenuAdapter = new LeftMenuAdapter(getContext(), datas);
    }

    private void initMenu() {
        datas.add(MenuEnum.CLEAN_TASK);
        datas.add(MenuEnum.VERSION_LIST);
        datas.add(LOOK_ID);
//        datas.add(CLEAN_SCRIPT);
        datas.add(SIGNATURE);
        datas.add(UNBIND);
    }

    public static LeftFragment newInstance() {

        Bundle args = new Bundle();
        LeftFragment fragment = new LeftFragment();
        fragment.setArguments(args);
        return fragment;
    }


}
