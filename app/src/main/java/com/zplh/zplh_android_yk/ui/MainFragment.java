package com.zplh.zplh_android_yk.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.TaskManager;
import com.zplh.zplh_android_yk.adapter.TaskAdapter;
import com.zplh.zplh_android_yk.bean.TaskBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import zplh.com.commonlibrary.utils.GsonUtils;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
public class MainFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.lv)
    RecyclerView lv;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefreshLayout;
    private TaskAdapter taskAdapter;

    @Override
    int getLayoutId() {
        return R.layout.main_fragment;
    }

    @Override
    protected void onFragmentVisibleChange(boolean isVisible, View view) {
        if (isVisible) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
            layoutManager.setStackFromEnd(true);
            lv.setLayoutManager(layoutManager);
            lv.setItemAnimator(new DefaultItemAnimator());
            lv.setAdapter(taskAdapter);
            mRefreshLayout.setOnRefreshListener(this);
        }
    }

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void onFragmentFirstVisible() {
        taskAdapter = new TaskAdapter(getContext(),new ArrayList<>());
        taskAdapter.setClicklistener(position -> {
            String msg = taskAdapter.getTaskList().get(position)
                    .getStateMsg();
            if (TextUtils.isEmpty(msg)) return;

            String string = GsonUtils.createGsonString(taskAdapter.getTaskList().get(position));
            ErrorUI.starter(getContext(),string);

        });

    }


    //刷新listView
    public void upDataTask(List<TaskBean> taskBeans) {
        if (taskAdapter!=null) {
            taskAdapter.setTaskList(taskBeans);
            taskAdapter.notifyDataSetChanged();
            if (taskAdapter.getItemCount() > 1) {
                lv.scrollToPosition(taskAdapter.getItemCount() - 1);
            }
        }
        }

    public void refresh(){
        if (taskAdapter!=null&&lv!=null) {
            taskAdapter.notifyDataSetChanged();
            if (taskAdapter.getItemCount() > 1) {
                lv.scrollToPosition(taskAdapter.getItemCount() - 1);
            }
        }
    }

    @Override
    public void onRefresh() {
        upDataTask(TaskManager.getInstance().getTaskArrays());
        mRefreshLayout.setRefreshing(false);

    }
}
