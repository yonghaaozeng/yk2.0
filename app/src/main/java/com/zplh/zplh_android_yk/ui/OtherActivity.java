package com.zplh.zplh_android_yk.ui;

import android.widget.Button;

import com.zplh.zplh_android_yk.R;

import butterknife.BindView;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/07/30
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class OtherActivity extends BaseActivity {
    @BindView(R.id.tv_1)
    Button mTv1;

    @Override
    int getLayoutId() {
        return R.layout.other_activity;
    }

    @Override
    void onFirstStart() {
            mTv1.setOnClickListener(v -> {
//                File file = new File(Environment.getExternalStorageDirectory(), "wxzs.apk");
//                new DownLoadModule().downLoadFile(this,file, NetConstant.DOWNLOADURL);
            });
    }
}
