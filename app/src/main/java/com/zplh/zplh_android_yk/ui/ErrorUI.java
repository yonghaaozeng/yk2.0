package com.zplh.zplh_android_yk.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zplh.zplh_android_yk.R;
import com.zplh.zplh_android_yk.bean.TaskBean;

import butterknife.BindView;
import zplh.com.commonlibrary.utils.GsonUtils;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/09/01
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class ErrorUI extends BaseActivity {
    @BindView(R.id.error_msg)
    TextView mErrorMsg;
    @BindView(R.id.iv)
    ImageView mIv;

    @Override
    int getLayoutId() {
        return R.layout.error_ui;

    }

    @Override
    void onFirstStart() {
        String bean = getIntent().getStringExtra("BEAN");
        TaskBean taskBean = GsonUtils.changeGsonToBean(bean, TaskBean.class);
        if (!TextUtils.isEmpty(taskBean.getStateMsg())){
            mErrorMsg.setText(taskBean.getStateMsg());
        }
        if (!TextUtils.isEmpty(taskBean.getError_path())){
            Bitmap bitmap = BitmapFactory.decodeFile(taskBean.getError_path());
            mIv.setImageBitmap(bitmap);
        }
    }

    public static void starter(Context context,String taskBean){
        Intent intent = new Intent(context, ErrorUI.class);
        intent.putExtra("BEAN",taskBean);
        context.startActivity(intent);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
