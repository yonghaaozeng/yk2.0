package com.zplh.zplh_android_yk.ui;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.ListView;

import com.zplh.zplh_android_yk.adapter.VersionAdapter;
import com.zplh.zplh_android_yk.bean.VersionBean;
import com.zplh.zplh_android_yk.intf.VersionListServices;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import io.reactivex.Observable;
import zplh.com.commonlibrary.R;
import zplh.com.commonlibrary.module.DownLoadModule;
import zplh.com.commonlibrary.networker.NetCustomSubscriber;
import zplh.com.commonlibrary.networker.NetWorker;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/08/09
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class VersionActivity  extends FragmentActivity{
    @BindView(R.id.lv_version)
    ListView version_lv;
    private VersionAdapter versionAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.version_ui);
        version_lv = (ListView) findViewById(R.id.lv_version);
        versionAdapter = new VersionAdapter(this);
        version_lv.setAdapter(versionAdapter);


        version_lv.setOnItemClickListener((parent, view, position, id) -> {
            VersionBean.DataBean item = versionAdapter.getItem(position);
            File file = new File(Environment.getExternalStorageDirectory(), "wxzs.apk");
            new DownLoadModule().downLoadFile(this,file,item.getUrl_address());

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        VersionListServices versionListServices = NetWorker.getInstance().getRetrofit().create(VersionListServices.class);
        Observable<VersionBean> phone = versionListServices.getList();
        NetWorker.getInstance().createNetAsync(phone, new NetCustomSubscriber<VersionBean>() {
            @Override
            public void onNext(VersionBean o) {
                List<VersionBean.DataBean> data = o.getData();
                versionAdapter.setBeans(data);
                versionAdapter.notifyDataSetChanged();
            }
        });
    }
}
