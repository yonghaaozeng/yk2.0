package com.zplh.zplh_android_yk.enumconstant;

import com.zplh.zplh_android_yk.R;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
public enum MenuEnum {
    CLEAN_TASK(0,"清除任务",R.drawable.clean),
    VERSION_LIST(1,"版本列表",R.drawable.version_list),
    LOOK_ID(2,"查看设备id",R.drawable.idcard),
//    CLEAN_SCRIPT(3,"清除所有脚本",R.drawable.clean_script),
    SIGNATURE(4,"个性签名",R.drawable.sing),
    UNBIND(5,"解除绑定",R.drawable.unbind);
    MenuEnum(int menuId, String des, int icon) {
        this.menuId = menuId;
        this.des = des;
        this.icon = icon;
    }

    int menuId;
    String des;
    int icon;

    public int getMenuId() {
        return menuId;
    }

    public MenuEnum setMenuId(int menuId) {
        this.menuId = menuId;
        return this;
    }

    public String getDes() {
        return des;
    }

    public MenuEnum setDes(String des) {
        this.des = des;
        return this;
    }

    public int getIcon() {
        return icon;
    }

    public MenuEnum setIcon(int icon) {
        this.icon = icon;
        return this;
    }




}
