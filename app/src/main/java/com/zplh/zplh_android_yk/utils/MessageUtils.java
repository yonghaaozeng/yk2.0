package com.zplh.zplh_android_yk.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.zplh.zplh_android_yk.UserManager;
import com.zplh.zplh_android_yk.bean.MessageBean;
import com.zplh.zplh_android_yk.bean.TaskBean;

import org.json.JSONException;
import org.json.JSONObject;

import zplh.com.commonlibrary.utils.GsonUtils;

/**
 * Created by yong hao zeng on 2018/7/6.
 */
public class MessageUtils {
    //将message转化为task
    public static TaskBean message2Task(@NonNull String messageBean){
        TaskBean taskBean = new TaskBean();
        try {
            MessageBean.ContentBean.DataBean dataBean = GsonUtils.changeGsonToBean(messageBean, MessageBean.ContentBean.DataBean.class);
            int task_id = dataBean.getTask_id();
            taskBean.setTaskId(task_id + "");
            taskBean.setTaskName(dataBean.getTask_name());
            taskBean.setLogId(dataBean.getLog_id());
            taskBean.setScriptVersion(Integer.valueOf(dataBean.getScript_id()));
            if (!TextUtils.isEmpty(dataBean.getTodo_time())&&!TextUtils.equals("0",dataBean.getTodo_time()))
            taskBean.setIsTimeTask(true);
            if (taskBean.getIsTimeTask()){
                taskBean.setDelayed_time(Long.valueOf(dataBean.getTodo_time()) * 1000);
            }
            taskBean.setSendTime(System.currentTimeMillis());
            taskBean.setAddress_url(dataBean.getAddress_url());
            taskBean.setMethodName(dataBean.getEntrance());
            taskBean.setPackageName(dataBean.getPackageName());
            JSONObject paramBean = new JSONObject();

            try {
                if (dataBean.getRange() != null) {
                    for (String s : dataBean.getRange()) {
                        if (!TextUtils.isEmpty(s) && s.contains(UserManager.getInstance().getMid())) {
                            String[] split = s.split("_");
                            if (split.length == 2) {
                                paramBean.put("accType", Integer.valueOf(split[1]));
                                taskBean.setAcctype(Integer.valueOf(split[1])+"");
                                break;
                            }
                        }
                    }
                }
                paramBean.put("log_id", taskBean.getLogId());
                paramBean.put("uid", UserManager.getInstance().getMid());
                paramBean.put("taskId",taskBean.getTaskId());
                JSONObject message = new JSONObject(messageBean);
                JSONObject param = message.getJSONObject("param");

                    try {
                        paramBean.put("partition",param.get("partition"));

                    }catch (Exception e){
                        paramBean.put("partition","api8");

                    }

                paramBean.put("params", param);
            } catch (JSONException e) {

            }
            taskBean.setDataGson(paramBean.toString());
            return taskBean;
        }catch (Exception e) {
        return taskBean;
        }
    }
}
