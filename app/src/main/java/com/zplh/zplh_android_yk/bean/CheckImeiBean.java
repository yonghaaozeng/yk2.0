package com.zplh.zplh_android_yk.bean;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class CheckImeiBean  {
    /**
     * ret : 200
     * data : 0513
     * msg : Already exist
     */

    private String ret = "";
    private String data = "";
    private String msg = "";

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
