package com.zplh.zplh_android_yk.bean;

/**
 * Created by yong hao zeng on 2018/7/10.
 */
public class ParamBean {
    int accType;
    String uid;
    Object params;
    public int getAccType() {
        return accType;
    }

    public ParamBean setAccType(int accType) {
        this.accType = accType;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public ParamBean setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Object getParams() {
        return params;
    }

    public ParamBean setParams(Object params) {
        this.params = params;
        return this;
    }
}
