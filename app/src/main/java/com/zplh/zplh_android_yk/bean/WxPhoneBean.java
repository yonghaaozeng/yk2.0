package com.zplh.zplh_android_yk.bean;

import java.util.List; /**
 * Created by Administrator on 2017/8/1.
 */

/**
 * 微信获取手机通讯录bean
 */

public class WxPhoneBean {


    /**
     * ret : 200
     * success : success
     * data : [{"name":"帅","phone":"15316055978"}]
     * custom : a
     */

    private String ret;
    private String success;
    private String custom;
    private List<DataBean> data;

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 帅
         * phone : 15316055978
         */

        private String name;
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
