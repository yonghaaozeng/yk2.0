package com.zplh.zplh_android_yk.bean;

import java.util.List;

/**
 * Created by yong hao zeng on 2018/7/6.
 */
public class MessageBean {

    /**
     * content : {"data":{"log_id":"553878","param":{"materia_ss":"人在路上，处处都是镜子|/Uploads/Picture/2018-07-06/153084857728254.jpg@哈哈哈········","script_id":"11"},"range":["1988_2","1989_1","1993_1","1992_1","1995_1","1997_1"],"task_id":2,"todo_time":"0"}}
     */

    private ContentBean content;

    public ContentBean getContent() {
        return content;
    }

    public void setContent(ContentBean content) {
        this.content = content;
    }

    public static class ContentBean {
        /**
         * data : {"log_id":"553878","param":{"materia_ss":"人在路上，处处都是镜子|/Uploads/Picture/2018-07-06/153084857728254.jpg@哈哈哈········","script_id":"11"},"range":["1988_2","1989_1","1993_1","1992_1","1995_1","1997_1"],"task_id":2,"todo_time":"0"}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * log_id : 553878
             * param : {"materia_ss":"人在路上，处处都是镜子|/Uploads/Picture/2018-07-06/153084857728254.jpg@哈哈哈········","script_id":"11"}
             * range : ["1988_2","1989_1","1993_1","1992_1","1995_1","1997_1"]
             * task_id : 2
             * todo_time : 0
             */
            private String packageName = "";
            private String entrance = "";//入口名
            private String log_id = "";
            private ParamBean param = new ParamBean();
            private String task_name = "";
            private int task_id;
            private String todo_time = "";
            private String script_id = "";
            private String address_url = "";
            private List<String> range;


            public String getAddress_url() {
                return address_url;
            }

            public DataBean setAddress_url(String address_url) {
                this.address_url = address_url;
                return this;
            }

            public String getTask_name() {
                return task_name;
            }

            public DataBean setTask_name(String task_name) {
                this.task_name = task_name;
                return this;
            }

            public String getScript_id() {
                return script_id;
            }

            public DataBean setScript_id(String script_id) {
                this.script_id = script_id;
                return this;
            }

            public String getPackageName() {
                return packageName;
            }

            public DataBean setPackageName(String packageName) {
                this.packageName = packageName;
                return this;
            }

            public String getEntrance() {
                return entrance;
            }

            public DataBean setEntrance(String entrance) {
                this.entrance = entrance;
                return this;
            }

            public String getLog_id() {
                return log_id;
            }

            public void setLog_id(String log_id) {
                this.log_id = log_id;
            }

            public ParamBean getParam() {
                return param;
            }

            public void setParam(ParamBean param) {
                this.param = param;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }

            public String getTodo_time() {
                return todo_time;
            }

            public void setTodo_time(String todo_time) {
                this.todo_time = todo_time;
            }

            public List<String> getRange() {
                return range;
            }

            public void setRange(List<String> range) {
                this.range = range;
            }

            public static class ParamBean {
                /**
                 * materia_ss : 人在路上，处处都是镜子|/Uploads/Picture/2018-07-06/153084857728254.jpg@哈哈哈········
                 * script_id : 11
                 */

                private String materia_ss;
                private String script_id;

                public String getMateria_ss() {
                    return materia_ss;
                }

                public void setMateria_ss(String materia_ss) {
                    this.materia_ss = materia_ss;
                }

                public String getScript_id() {
                    return script_id;
                }

                public void setScript_id(String script_id) {
                    this.script_id = script_id;
                }
            }
        }
    }
}
