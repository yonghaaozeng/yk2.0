package com.zplh.zplh_android_yk.bean;

/**
 * Created by yong hao zeng on 2018/7/11.
 */
public class ScriptStateBean {
    String log_id;
    String msg;

    public String getLog_id() {
        return log_id;
    }

    public ScriptStateBean setLog_id(String log_id) {
        this.log_id = log_id;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public ScriptStateBean setMsg(String msg) {
        this.msg = msg;
        return this;
    }
}
