package com.zplh.zplh_android_yk.bean;

import java.util.List;

/**
 * Created by yong hao zeng on 2018/6/7/007.
 */
public class VersionBean {

    /**
     * msg : success
     * ret : 200
     * data : [{"id":"1","version_number":"1.73692","url_address":"http://58.49.28.78:8087/download/wxzs1.73692.apk","add_time":"2018-06-07 09:24:01"},{"id":"2","version_number":"1.7369","url_address":"http://58.49.28.78:8087/download/wxzs1.7369.apk","add_time":"2018-06-07 09:24:01"},{"id":"3","version_number":"1.7368","url_address":"http://58.49.28.78:8087/download/wxzs1.7368.apk","add_time":"2018-06-07 09:24:01"},{"id":"4","version_number":"1.7366","url_address":"http://58.49.28.78:8087/download/wxzs1.7366.apk","add_time":"2018-06-07 09:24:01"},{"id":"5","version_number":"1.7365","url_address":"http://58.49.28.78:8087/download/wxzs1.7365.apk","add_time":"2018-06-07 09:24:01"}]
     */

    private String msg;
    private int ret;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * version_number : 1.73692
         * url_address : http://58.49.28.78:8087/download/wxzs1.73692.apk
         * add_time : 2018-06-07 09:24:01
         */

        private String id;
        private String version_number;
        private String url_address;
        private String add_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVersion_number() {
            return version_number;
        }

        public void setVersion_number(String version_number) {
            this.version_number = version_number;
        }

        public String getUrl_address() {
            return url_address;
        }

        public void setUrl_address(String url_address) {
            this.url_address = url_address;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }
    }
}
