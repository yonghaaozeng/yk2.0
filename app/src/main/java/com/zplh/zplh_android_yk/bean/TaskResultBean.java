package com.zplh.zplh_android_yk.bean;

/**
 * Created by yong hao zeng on 2018/7/7.
 */
public class TaskResultBean {



    int applyState;//执行状态 0成功 1失败
    TaskBean taskBean;
    String errorMsg;

    public int getApplyState() {
        return applyState;
    }

    public TaskResultBean setApplyState(int applyState) {
        this.applyState = applyState;
        return this;
    }

    public TaskBean getTaskBean() {
        return taskBean;
    }

    public TaskResultBean setTaskBean(TaskBean taskBean) {
        this.taskBean = taskBean;
        return this;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public TaskResultBean setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }
}
