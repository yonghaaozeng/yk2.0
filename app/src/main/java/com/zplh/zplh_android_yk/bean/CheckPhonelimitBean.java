package com.zplh.zplh_android_yk.bean;

/**
 * <pre>
 *     author : YONGHAOZENG
 *     e-mail : 1007687534@qq.com
 *     time   : 2018/09/14
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public class CheckPhonelimitBean {
    /**
     * ret : 200
     * msg : 正常加粉
     * data : {"already":13,"still":12}
     */

    private String ret;
    private String msg;
    private DataBean data;

    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * already : 13
         * still : 12
         */

        private int already;
        private int still;

        public int getAlready() {
            return already;
        }

        public void setAlready(int already) {
            this.already = already;
        }

        public int getStill() {
            return still;
        }

        public void setStill(int still) {
            this.still = still;
        }
    }
}
