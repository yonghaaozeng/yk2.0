package com.zplh.zplh_android_yk.bean;

import android.annotation.SuppressLint;

import com.zplh.zplh_android_yk.R;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by yong hao zeng on 2018/7/3.
 */
@Entity
public class TaskBean {
    @Transient
    public static final int STATE_SUCCESS = 1;
    @Transient
    public static final int STATE_WAIT = 0;
    @Transient
    public static final int STATE_ERROR = -1;
    @Property(nameInDb = "TASK_NAME")
    private String taskName = "";
    @Property(nameInDb = "SEND_TIME")
    private long sendTime;
    @Property(nameInDb = "END_TIME")
    private long endTime ;
    @Property(nameInDb = "TASK_ID")
    private String taskId = "";
    @Id @Unique
    private String logId = "";
    @Property(nameInDb = "STATE")
    private int state;
    @Property(nameInDb = "DATA")
    private String dataGson = "";
    @Property(nameInDb = "IS_TIME")
    private boolean isTimeTask;
    @Property(nameInDb = "DELAYED_TIME")
    private long delayed_time;
    @Property(nameInDb = "SC_VERSION")
    private int scriptVersion;
    @Property(nameInDb = "PACKAGE_NAME")
    private String packageName = "";
    @Property(nameInDb = "METHOD_NAME")
    private String methodName = "";
    @Property(nameInDb = "STATE_MSG")
    private String stateMsg = "";
    @Property(nameInDb = "error_path")
    private String error_path;
    @Property(nameInDb = "address_url")
    private String address_url = "";

    public String getAddress_url() {
        return address_url;
    }

    public TaskBean setAddress_url(String address_url) {
        this.address_url = address_url;
        return this;
    }

    public String getError_path() {
        return error_path;
    }

    public TaskBean setError_path(String error_path) {
        this.error_path = error_path;
        return this;
    }

    public String getAcctype() {
        return acctype;
    }

    public void setAcctype(String acctype) {
        this.acctype = acctype;
    }

    @Transient
    private String acctype;



    @Generated(hash = 388160517)
    public TaskBean(String taskName, long sendTime, long endTime, String taskId, String logId,
            int state, String dataGson, boolean isTimeTask, long delayed_time, int scriptVersion,
            String packageName, String methodName, String stateMsg, String error_path,
            String address_url) {
        this.taskName = taskName;
        this.sendTime = sendTime;
        this.endTime = endTime;
        this.taskId = taskId;
        this.logId = logId;
        this.state = state;
        this.dataGson = dataGson;
        this.isTimeTask = isTimeTask;
        this.delayed_time = delayed_time;
        this.scriptVersion = scriptVersion;
        this.packageName = packageName;
        this.methodName = methodName;
        this.stateMsg = stateMsg;
        this.error_path = error_path;
        this.address_url = address_url;
    }

    @Generated(hash = 1443476586)
    public TaskBean() {
    }


    public String getStateMsg() {
        return stateMsg;
    }

    public void setStateMsg(String stateMsg) {
        this.stateMsg = stateMsg;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    //返回格式化的时间 0是开始 1是结束时间 对于定时任务 开始时间是todotime
    public String[] getFormatTime(){
        String[] times = new String[2];

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format=new SimpleDateFormat("dd HH:mm");
        Date d1;
        if (!isTimeTask){
        d1 = new Date(getSendTime());}
        else{
            d1 = new Date(getDelayed_time());
        }
        String startTime=format.format(d1);
        times[0] = startTime;

        if (getEndTime() == 0){
            times[1] = "未开始执行";
        }else {
            Date d2 = new Date(getEndTime());
            String endTime = format.format(d2);
            times[1] = endTime;
        }
        return times;
    }

    public int getStateIcon(){
        switch (getState()){
            case STATE_WAIT:
                return R.drawable.task_wait;
            case STATE_ERROR:
                return R.drawable.task_error;
            case STATE_SUCCESS:
                return R.drawable.task_success;
        }
        return R.drawable.ic_launcher;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public int getScriptVersion() {
        return scriptVersion;
    }

    public TaskBean setScriptVersion(int scriptVersion) {
        this.scriptVersion = scriptVersion;
        return this;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getDataGson() {
        return this.dataGson;
    }

    public void setDataGson(String dataGson) {
        this.dataGson = dataGson;
    }

    public boolean getIsTimeTask() {
        return this.isTimeTask;
    }

    public void setIsTimeTask(boolean isTimeTask) {
        this.isTimeTask = isTimeTask;
    }

    public long getDelayed_time() {
        return this.delayed_time;
    }

    public void setDelayed_time(long delayed_time) {
        this.delayed_time = delayed_time;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }




}
