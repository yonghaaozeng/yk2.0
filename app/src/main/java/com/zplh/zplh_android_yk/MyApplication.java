package com.zplh.zplh_android_yk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.alibaba.sdk.android.push.CloudPushService;
import com.alibaba.sdk.android.push.CommonCallback;
import com.alibaba.sdk.android.push.noonesdk.PushServiceFactory;

import java.util.LinkedList;

import cn.jpush.android.api.JPushInterface;
import zplh.com.commonlibrary.LibraryManager;
import zplh.com.commonlibrary.MyExceptionHandler;
import zplh.com.commonlibrary.networker.NetWorker;
import zplh.com.commonlibrary.networker.NetWorkerConfig;
import zplh.com.commonlibrary.utils.LogUtils;


/**
 * Created by yong hao zeng on 2018/6/29.
 */
public class MyApplication extends Application {
    private static LinkedList<Activity> uiArrays = new LinkedList<>();
    public static Context context;
    private static final String ALITAG = "ALI_INIT";
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        registerActivityLifecycleCallbacks(new UILifeCycleCallback());
        if (!BuildConfig.DEBUG) {
            MyExceptionHandler.getInstance().init(this);
        }
        initLibrary();
        initNetWorker();
        initPush();
    }

    private void initNetWorker() {
        NetWorker.init(NetWorkerConfig.getCustomConfig());
    }

    private void initLibrary() {
        LibraryManager.init(this, BuildConfig.DEBUG);
    }


    //退出程序
    public static void exitApplication(){
        for (Activity uiArray : uiArrays) {
            uiArray.finish();
        }
    }

    //返回当前activity
    public static Context getCurrentActivity(){
         return  uiArrays.getLast();
    }

    //各种推送初始化
    private void initPush() {


        JPushInterface.init(this);
        String id = JPushInterface.getRegistrationID(this);
        initCloudChannel(this);
    }

    private void initCloudChannel(MyApplication application) {
        PushServiceFactory.init(application);
        CloudPushService pushService = PushServiceFactory.getCloudPushService();
        pushService.register(application, new CommonCallback() {
            @Override
            public void onSuccess(String response) {
                LogUtils.d(ALITAG, "init cloudchannel success");
            }
            @Override
            public void onFailed(String errorCode, String errorMessage) {
                Log.d(ALITAG, "init cloudchannel failed -- errorcode:" + errorCode + " -- errorMessage:" + errorMessage);
            }
        });
    }


    //activity生命周期回调
     private class UILifeCycleCallback implements ActivityLifecycleCallbacks{

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
                uiArrays.add(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            uiArrays.remove(activity);
        }
    }

}
