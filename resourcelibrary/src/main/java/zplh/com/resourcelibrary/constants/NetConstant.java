package zplh.com.resourcelibrary.constants;

import android.text.TextUtils;

/**
 * Created by yong hao zeng on 2018/6/30.
 */
public class NetConstant  {
   public static final String TYPE_URL = ".shujucm.com.cn:8087/yk/index.php/";
   public static final String HOST_ADDRESS = "http://load1.shujucm.com.cn:8087/yk/index.php/";
   public static final String CHECK_BINDING = "home/binding/check_imei";
   public static final String BINDING = "home/binding/index";
   public static final String SING = "home/ApiAndroid/text_materials";
   public static final String SEND_RECEIVE = "home/ApiAndroid/updata_task_status";
   public static final String GET_PHONE = "http://api7.shujucm.com.cn:8087/api_wechat/index_new.php";
   public static final String SEND_COMPLETE = "home/ApiAndroid/hasExecutedDevices";
   public static final String VERSION_LIST = "http://load1.shujucm.com.cn:8087/yk/index.php/home/ApiAndroid/getVersion";
   public static final String CHECK_LIMIT = "home/ApiAndroid/applyLimit_new";

   /**
    * 根据地区分布
    * @param type
    * @return
    */
   public static String getUrl(String type,String url){
        if (TextUtils.equals(type,"0")){
            return HOST_ADDRESS+url;

        }
      String result = "http://"+type+TYPE_URL;
        return result+url;
   }

   public static String getChecklImit(String type){
       String result = "http://"+type+TYPE_URL+CHECK_LIMIT;
       return result;
   }
}
