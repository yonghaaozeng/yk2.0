package zplh.com.resourcelibrary.constants;

/**
 * Created by yong hao zeng on 2018/4/16/016.
 */
public class TaskConstant {
    //0,1,15,16 17 18 19 20 21 22 23 24 25 26
    public static final int TASK_WX_FRIENDS_DS = 1; //朋友圈点赞
    public static final int TASK_WX_FRIEND_PIC_TEXT_SEND = 2;//朋友圈图文发布
    public static final int TASK_WX_SUM_FRIENDS = 15;//统计好友数量
    public static final int TASK_WX_SAVE_SIGNATURE = 16;//修改个性签名
    public static final int TASK_WX_SEND_GZH = 17;//发送公众号名片
    public static final int TASK_WX_FRIENDS_GAME = 18;//朋友圈启动游戏
    public static final int TASK_WX_FRIENDS_SHOP = 19;//朋友圈进入京东购物
    public static final int TASK_WX_FRIENDS_VEDIO = 20;//朋友圈小视频发布
    public static final int TASK_WX_MASS_VEDIO = 21;//好友发视频
    public static final int TASK_WX_CROWD_VEDIO = 22;//微信群发视频
    public static final int TASK_WX_ADD_XT = 23;//嗅探加好友
    public static final int TASK_WX_ADD_CROWD = 24;//拉群任务
    public static final int TASK_WX_COUNT = 25;//统计好友和群成员
    public static final int TASK_WX_CONTACTS_ADD_BIG = 26;//通讯录加高权重号
    public static final int TASK_WX_ONE_MSG = 10;//好友逐个发图片
    public static final int TASK_WX_CROWD_MSG = 11;//微信群发消息
    public static final int TASK_WX_GO_XIAO_CHENG_XU = 59;//进入小程序
    public static final int TASK_WX_QUN_TU_WEN = 30;//微信群发图文
    public static final int TASK_WX_SHOU_FU_KUAN = 61;//收付款
    public static final int TASK_WX_TONG_JI_ALL = 25;//群成员 好友统计
    public static final int TASK_WX_COLLECT_FR = 54;//收藏朋友圈内容
    public static final int TASK_WX_READ_FRIEND_CIRCLE = 52;//阅读朋友圈
    public static final int TASK_WX_LOOK_FR_CIRCLE = 53;//查看好友朋友圈
    public static final int TASK_WX_SETTING = 27;//微信通用设置
    public static final int TASK_WX_PHONE_SET = 28;//微信手机设置
    public static final int TASK_WX_CROWD_TUWEN = 30;//微信群发图文
    public static final int TASK_WX_HAVENO = 31;//养号互聊
    public static final int TASK_WX_INIT = 32;//微信初始化
    public static final int TASK_WX_TIME_START = 33;//微信定时开关
    public static final int TASK_WX_CHECK_COLLECT_CONTENT = 55;//查看收藏内容
    public static final int TASK_WX_READ_TENCENT_NEWS = 56;//阅读腾讯新闻
    public static final int TASK_WX_TOP_STORIES = 57;//启用看一看
    public static final int TASK_WX_USER_SEARCH = 58;//启用搜一搜
    public static final int TASK_WX_CHECK_WALLET = 60;//    查看零钱明细
    public static final int TASK_WX_FIND_DEV = 37;//微信寻找设备
    public static final int Task_WX_ADD_FRIEND = 5;//通讯录加好友
    public static final int Task_WX_REMARK = 29;//修改备注
    public static final int Task_WX_SUPER_REMARK = 68;//超级修改备注
    public static final int Task_WX_CUSTOM_LAQUN = 47;//群名称拉群 （自定义拉群）
    public static final int Task_WX_SUM_FRIENDS = 502;//好友统计
    public static final int TASK_MOCK_LOCATION = 75;//模拟定位
    public static final int TASK_CLEAR = 72;
    public static final int TASK_WX_QR_LAQUN = 69; //自定义二维码拉群
    public static final int TASK_WX_QUN_FA_HONGHBAO = 77; //群红包
    public static final int TASK_WX_QUN_SHOU_HONGHBAO = 78; //群红包
    public static final int TASK_WX_MOMENTS_COMMENT = 79;//朋友圈评论
    public static final int TASK_WX_DELETE_FRIEND = 83; //删除ZZZZ好友
    public static final int TASK_UPDATA = 73;//版本更新
    public static final int Task_MONEY_DETAIL = 60;//查看零钱明细

    public static String getTaskNameForID(int taskId) {
        if (taskId == 1) {
            return"朋友圈点赞";
        } else if (taskId == 2) {
            return"朋友圈图文发布";
        } else if (taskId == 3) {
            return"分享链接";
        } else if (taskId == 4) {
            return"搜索加好友";
        } else if (taskId == 5) {
            return"通讯录加好友";
        } else if (taskId == 6) {
            return"嗅探加好友";
        } else if (taskId == 7) {
            return"好友发消息";
        } else if (taskId == 8) {
            return"好友发图片";
        } else if (taskId == 9) {
            return"好友逐个发消息";
        } else if (taskId == 10) {
            return"好友逐个发图片";
        } else if (taskId == 11) {
            return"微信群发消息";
        } else if (taskId == 12) {
            return"微信群发图片";
        } else if (taskId == 13) {
            return"微信群发名片";
        } else if (taskId == 14) {
            return"自动通过好友申请";
        } else if (taskId == 15) {
            return"统计好友数量";
        } else if (taskId == 16) {
            return"修改个性签名";
        } else if (taskId == 17) {
            return"发送公众号名片";
        } else if (taskId == 18) {
            return"朋友圈启动游戏";
        } else if (taskId == 19) {
            return"朋友圈进入京东购物";
        } else if (taskId == 20) {
            return"朋友圈小视频发布";
        } else if (taskId == 21) {
            return"好好友发视频";
        } else if (taskId == 22) {
            return"微信群发视频";
        } else if (taskId == 26) {
            return"通讯录加高权重号";
        } else if (taskId == 25) {
            return"统计好友和群成员";
        } else if (taskId == 24) {
            return"微信拉群";
        } else if (taskId == 27) {
            return"微信通用设置";
        } else if (taskId == 28) {
            return"微信手机设置";
        } else if (taskId == 29) {
            return"自定义修改备注";
        } else if (taskId == 30) {
            return"微信群发图文";
        } else if (taskId == 31) {
            return"微信养号互聊";
        } else if (taskId == 32) {
            return"手机设置初始化";
        } else if (taskId == 33) {
            return"手机定时开机和关机";
        } else if (taskId == 500) {
            return"支付宝搜索加好友";
        } else if (taskId == 501) {
            return"支付宝拉群";
        } else if (taskId == 502) {
            return"支付宝好友统计";
        } else if (taskId == 503) {
            return"支付宝群好友统计";
        } else if (taskId == 504) {
            return"支付推送APK到手机";
        } else if (taskId == 505) {
            return"支付宝通用设置";
        } else if (taskId == 506) {
            return"支付宝手机设置";
        } else if (taskId == 507) {
            return"支付宝初始化任务";
        } else if (taskId == 508) {
            return"支付宝设置定时开关机";
        } else if (taskId == 509) {
            return"支付宝群图文发布";
        } else if (taskId == 34) {
            return"微信语音聊天";
        } else if (taskId == 35) {
            return"微信视频聊天";
        } else if (taskId == 37) {
            return"微信寻找设备";
        } else if (taskId == 510) {
            return"支付宝寻找手机";
        } else if (taskId == 511) {
            return"更新版本";
        } else if (taskId == 38) {
            return"浏览公众号";
        } else if (taskId == 39) {
            return"关注公众号";
        } else if (taskId == 40) {
            return"版本更新";
        } else if (taskId == 41) {
            return"双向互聊";
        } else if (taskId == 42) {
            return"微信发红包";
        } else if (taskId == 43) {
            return"上传好友信息";
        } else if (taskId == 44) {
            return"好友直接群发";
        } else if (taskId == 45) {
            return"群直接群发";
        } else if (taskId == 46) {
            return"二维码拉群";
        } else if (taskId == 47) {
            return"群名称拉群";
        } else if (taskId == 51) {
            return"向好友发位置";
        } else if (taskId == 48) {
            return"阅读新消息并回复";
        } else if (taskId == 55) {
            return"版本回退";
        } else if (taskId == 55) {
            return"查看收藏内容";
        } else if (taskId == 56) {
            return"阅读腾讯新闻";
        } else if (taskId == 58) {
            return"启用搜一搜并使用";
        } else if (taskId == 59) {
            return"搜索进入小程序";
        } else if (taskId == 69) {
            return"自定义二维码拉群";
        } else if (taskId == 497) {
            return"被匹配的语音通话";
        } else if (taskId == 498) {
            return"被匹配的视频通话";
        } else if (taskId == 499) {
            return"双向互聊";
        } else if (taskId == 65) {
            return"语音互聊";
        } else if (taskId == 66) {
            return"删除朋友圈指定内容";
        } else if (taskId == 68) {
            return"自定义修改备注";
        } else if (taskId == 71) {
            return"查看已有小程序";
        } else if (taskId == 72){
            return "重置程序";
        } else if (taskId == 73){
            return "版本回退";
        }else if (taskId == 70) {
            return"ZZZ9加大号";
        } else if (taskId == 74) {
            return"备注整理";
        } else if (taskId == 76) {
            return"好友信息采集";
        } else if (taskId == 80) {
            return"通过ZZZ9好友申请";
        } else if (taskId == 81) {
            return"ZZZ9加大号";
        } else if (taskId == 82) {
            return"微信群发图文";
        } else if (taskId == 496) {
            return"通过ZZZ9好友申请";
        } else if (taskId == 513) {
            return"支付宝群图文发布";
        } else if (taskId == 515) {
            return"关注支付宝生活号";
        } else if (taskId == 516) {
            return"支付宝生活圈点赞";
        } else if (taskId == 517) {
            return"生活圈图文发布";
        } else if (taskId == 518) {
            return"生活圈小视频发布";
        } else if (taskId == 519) {
            return"支付宝获取名字";
        } else if (taskId == 520) {
            return"好友直接群发图文";
        } else if (taskId == 521) {
            return"群直接群发图文";
        } else if (taskId == 522) {
            return"终止所有任务";
        } else if (taskId == TaskConstant.TASK_WX_MOMENTS_COMMENT) {
            return"执行任务 朋友圈评论";
        } else if (taskId == TaskConstant.TASK_WX_QUN_FA_HONGHBAO) {
            return"群聊发红包";
        } else if (taskId == TaskConstant.TASK_WX_QUN_SHOU_HONGHBAO) {
            return"群聊收红包和群付款";
        } else if (taskId == TaskConstant.TASK_WX_DELETE_FRIEND) {
            return"删除好友";
        } else if (taskId == TaskConstant.TASK_WX_TOP_STORIES) {
            return"启用看一看并使用";
        } else if (taskId == TaskConstant.Task_MONEY_DETAIL) {
            return"查看零钱明细";
        } else {
            return"未知任务(" + taskId + ")";
        }
    }

}
